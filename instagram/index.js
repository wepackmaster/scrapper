const Scrapy = require('./scrapper')
const config = require('./config')
const schedule = require('node-schedule');
const mongoose = require('mongoose');
const fs = require('fs');
const util = require('util');
const log_file = fs.createWriteStream(__dirname + '/debug.log', {flags : 'w'});
const log_stdout = process.stdout;
// const rule = new schedule.RecurrenceRule();

console.log = function(d) { //
  fs.appendFile('debug.log',util.format(d)+ '\n',()=>{});
};
main()

async function main(){
    console.log("scrapping started "+ new Date())
    const len = config.source.length; 
    for(let index=0;index<len;index++){
      let path = config.source[index]
      console.log("scrapping started",path)
      let scrapy = new Scrapy(path)
      await scrapy.start(path).catch(error => console.error(error+new Date()))
    }
}

const job = schedule.scheduleJob('* * 1 * * *', function(){
    console.log('Will run after every 12 hours');
  mongoose.connect(config.uri, {useNewUrlParser: true},(error)=>{
    if(error)
    { console.log(error+new Date()); 
      return
    }
    else main();
  });
});


