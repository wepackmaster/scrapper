const chromium = require("chrome-aws-lambda");
const Data = require('./schema')
const uuidv4 = require('uuid/v4');

class Scrapy {
  constructor(
    path = `instagram`,
    data,
    host = `https://www.instagram.com/explore/tags/`
  ) {
    this.path = path;
    this.host = host;
    this.data = data;
  }
  get url() {
    return `${this.host}${this.path}`;
  }
  async start() {
    console.log("srcapping"+this.url);
    const browser = await chromium.puppeteer.launch({
      args: chromium.args,
      defaultViewport: chromium.defaultViewport,
      executablePath: await chromium.executablePath,
      headless: true
    });
    this.browser = browser;
    const page = await this.browser.newPage();
    this.page = page;
    await this.page.setExtraHTTPHeaders({
      "Accept-Language": "en-US"
    });

    await this.page.goto(this.url, {
      waitUntil: `networkidle0`
    });

    if (await this.page.$(`.dialog-404`)) {
      console.log(`The url you followed may be broken`);
    }
    this.evaluate();
  }

  async evaluate() {
    try {
      this.items = await this.load(1000);
    } catch (error) {
      console.log(`There was a problem parsing the page`);
      // process.exit();
    }
    console.log(`Scraped ${this.items.size} posts`);
    this.buildJSON();
    await this.page.close();
    await this.browser.close();
  }

  async load(maxItemsSize) {
    this.maxItemsSize = maxItemsSize;
    var page = this.page;
    let previousHeight;
    var media = new Set();
    while (maxItemsSize == null || media.size < maxItemsSize) {
      try {
        previousHeight = await page.evaluate(`document.body.scrollHeight`);
        await page.evaluate(`window.scrollTo(0, document.body.scrollHeight)`);
        await page.waitForFunction(
          `document.body.scrollHeight > ${previousHeight}`
        );
        await page.waitFor(9000);
        const nodes = await page.evaluate(() => {
          const images = document.querySelectorAll(`a > div > div.KL4Bh > img`);
          return [].map.call(images, img => img.src);
        });

        nodes.forEach(element => {
          if (media.size < maxItemsSize) {
            media.add(element);
          }
        });
      } catch (error) {
        console.error(error);
        break;
      }
    }
    return media;
  }

  buildJSON() {
    var tmp = [];
    this.items.forEach((url) => {
      let image = `image_new${uuidv4()}.jpg`;
      let obj = {
        Top: {
          box: [],
          style: "",
          pattern: "",
          length: "",
          colors: { LAB: [] },
          material: "",
          detail: ""
        },
        name: image,
        Bottom: {
          box: [],
          colors: { LAB: [] },
          type: "",
          pattern: "",
          length: "",
          material: "",
          detail: "",
          skirt_type: ""
        },
        img_url: url,
        verified_tags: {
          verified_tags: [
            {
              top: {
                style: "",
                sleeve: "",
                pattern: "",
                material: "",
                length: "",
                detail: "",
                colors: { colors: { LAB: [] } }
              },
              bottom: {
                type: "",
                fit: "",
                pattern: "",
                material: "",
                detail: "",
                length: "",
                colors: { colors: { LAB: [] } }
              },
              inappropriate: false,
              swimwear: false,
              sleepwear: false,
              Celebrity: false,
              logo: false,
              notwoman: false
            },
            {
              top: {
                style: "",
                neck: "",
                sleeve: "",
                detail: "",
                pattern: "",
                length: "",
                material: "",
                colors: { colors: { LAB: [] } }
              },
              bottom: {
                type: "",
                length: "",
                skirt_type: "",
                detail: "",
                material: "",
                pattern: "",
                colors: { colors: { LAB: [] } }
              },
              inappropriate: false,
              swimwear: false,
              sleepwear: false,
              Celebrity: false,
              logo: false,
              notwoman: false
            }
          ],
          verified_by: []
        },
        reviewed_tags: {
          top: {
            style: "",
            neck: "",
            sleeve: "",
            length: "",
            pattern: "",
            detail: "",
            material: ""
          },
          bottom: {
            ankle: "",
            fit: "",
            type: "",
            length: "",
            pattern: "",
            detail: "",
            material: ""
          },
          reviewed_by: ""
        },
        classification: {
          classified: { top: 0, bottom: 0 },
          on_classification: { top: 0, bottom: 0 },
          classified_as: { top: "", bottom: "" }
        },
        common: {
          color: {
            top: { done: 0, on_process: 0, value: "" },
            bottom: { done: 0, on_process: 0, value: "" }
          }
        },
        features: {
          featured: {
            top: {
              neck: 0,
              sleeve: 0,
              length: 0,
              pattern: 0,
              detail: 0,
              material: 0
            },
            bottom: {
              ankle: 0,
              fit: 0,
              type: 0,
              length: 0,
              pattern: 0,
              detail: 0,
              material: 0
            }
          },
          on_features: {
            top: {
              neck: 0,
              sleeve: 0,
              length: 0,
              pattern: 0,
              detail: 0,
              material: 0
            },
            bottom: {
              ankle: 0,
              fit: 0,
              type: 0,
              length: 0,
              pattern: 0,
              detail: 0,
              material: 0
            }
          },
          featured_as: {
            top: {
              neck: "",
              sleeve: "",
              length: "",
              pattern: "",
              detail: "",
              material: ""
            },
            bottom: {
              ankle: "",
              fit: "",
              type: "",
              length: "",
              pattern: "",
              detail: "",
              material: ""
            }
          }
        },
        validity: { validated: 0, on_validation: 0, invalid: 0, reason: "" },
        createdOn : Date.now(),
        "from" : "instagram"
      };

      const recs = Data.update({img_url: url},obj,{upsert: true});
      recs.then(() => {});

    });
  }
}

module.exports = Scrapy;
