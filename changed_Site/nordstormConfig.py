#!/usr/bin/env python3
# Parameters
# database : url connection of mongodb
# parseUrl : url of clothes need to parse
# baseUrl : base url for nordstorm
#

config = {
    'database': 'mongodb://localhost:27017/',
    'parseUrl': 'https://shop.nordstrom.com/c/womens-clothing',
    'baseUrl': 'https://shop.nordstrom.com',
    'chromeDriverPath': './chrome/chromedriver'
}
