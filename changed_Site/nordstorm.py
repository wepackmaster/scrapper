from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
from pymongo import MongoClient
from nordstormConfig import config as cfg


client = MongoClient(cfg["database"])
db = client.test_database
testCol = db.wallmart

options = Options()
options.add_argument('--headless')
options.add_argument('--disable-gpu')
driver = webdriver.Chrome(cfg['chromeDriverPath'], chrome_options=options)
parseUrl = cfg["parseUrl"]
while(True):
    print("new")
    driver.get(parseUrl)
    html_doc = driver.page_source
    soup = BeautifulSoup(html_doc, 'html5lib')
    prodArr = soup.select("article:is(._1AOd3.QIjwE)")
    print(len(prodArr))
    for prod in prodArr :
        prodUrl = prod.findChild("a")
        prodUrl = prodUrl["href"]
        productUrl = cfg["baseUrl"] + prodUrl
        driver.get(productUrl)
        html_doc_product = driver.page_source
        soup_product = BeautifulSoup(html_doc, 'html5lib')
        imageUrlDiv = soup_product.select_one("div:is(._3wZ30._1rD0q)")
        print(imageUrlDiv)
        # print(imageUrlContainer)
        print("============================================",end="\n")
        
        
driver.quit()
