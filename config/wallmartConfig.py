#!/usr/bin/env python3
# Parameters
# database : url connection of mongodb
# parseUrl : url of clothes need to parse
# baseUrl : base url for wallmart
#

config = {
    'database': 'mongodb://localhost:27017/',
    'parseUrl': 'https://www.walmart.com/browse/clothing/mens-polos/5438_133197_1224680?povid=133197+%7C+2018-04-30+%7C+Mens_LHN_Clothing_Polos',
    'baseUrl': 'https://www.walmart.com',
    'chromeDriverPath': './chrome/chromedriver',
    "category": "tshirts polos",
    "type": "top",
    "gender": "male"
}


# women

# 1. https://www.walmart.com/browse/clothing/women-s-jackets-outerwear/5438_133162_163847?povid=FashionTopNav_Women_Clothing_CoatsJackets
# 2 https://www.walmart.com/browse/clothing/womens-dresses-jumpsuits/5438_133162_3074670?povid=FashionTopNav_Women_Clothing_DressesJumpsuits
# 3 https://www.walmart.com/browse/clothing/women-s-sweaters-cardigans/5438_133162_1199498?povid=FashionTopNav_Women_Clothing_Sweaters
# 4 https://www.walmart.com/browse/clothing/womens-sweatshirts-hoodies/5438_133162_8956587?povid=FashionTopNav_Women_Clothing_SweatshirtsHoodies
# 5 https://www.walmart.com/browse/clothing/womens-tops-t-shirts/5438_133162_2290732?povid=FashionTopNav_Women_Clothing_TopsT-Shirts

# 1 https://www.walmart.com/browse/clothing/mens-polos/5438_133197_1224680?povid=133197+%7C+2018-04-30+%7C+Mens_LHN_Clothing_Polos
# 2 https://www.walmart.com/browse/clothing/mens-casual-shirts/5438_133197_1224676?povid=FashionTopNav_Men_Clothing_CasualButtonDownShirts
# 3 https://www.walmart.com/browse/clothing/mens-dress-shirts/5438_133197_6970290?povid=FashionTopNav_Men_Clothing_DressShirts
#  4 https://www.walmart.com/browse/clothing/mens-graphic-tees/5438_133197_9358077?povid=133197+%7C++%7C+Mens_LHN_Clothing_Graphic_Tees

# 5 https://www.walmart.com/browse/clothing/mens-jackets-outerwear/5438_133197_5585888?povid=FashionTopNav_Men_Clothing_JacketsOuterwear

# 6 https://www.walmart.com/browse/clothing/mens-sweaters/5438_133197_5281276?povid=FashionTopNav_Men_Clothing_Sweaters

# 7 https://www.walmart.com/browse/clothing/mens-sweatshirts-hoodies/5438_133197_4546464?povid=FashionTopNav_Men_Clothing_SweatshirtsHoodies

# 8 https://www.walmart.com/browse/clothing/mens-t-shirts-tank-tops/5438_133197_4237948?povid=FashionTopNav_Men_Clothing_TShirtsTanks
