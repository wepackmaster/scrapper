#!/usr/bin/env python3
# Parameters
# database : url connection of mongodb
# parseUrl : url of clothes need to parse
# baseUrl : base url for nordstorm
#

config = {
    'database': 'mongodb://localhost:27017/',
    'parseUrl': 'https://www2.hm.com/en_us/women/products/tops/t-shirts.html/',
    'baseUrl': 'https://www2.hm.com',
    'chromeDriverPath': './chrome/chromedriver'
}
