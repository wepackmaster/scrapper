#!/usr/bin/env python3
# Parameters
# database : url connection of mongodb
# parseUrl : url of clothes need to parse
# baseUrl : base url for wallmart
#

config = {
    'database': 'mongodb://localhost:27017/',
    'parseUrl': 'https://www.zara.com/us/en/woman-jackets-l1114.html?v1=1281521',
    'baseUrl': 'https://www.zara.com',
    'chromeDriverPath': './chrome/chromedriver',
    "category": "top",
    "type": "tshirt"
}
