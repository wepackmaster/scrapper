from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
from pymongo import MongoClient
from config.myntraConfig import config as cfg
import re
import uuid
import cssutils

client = MongoClient(cfg["database"])
db = client.social
testCol = db.social_men
options = Options()
options.add_argument('--headless')  # comment this line to run on head mode
options.add_argument('--disable-gpu')
driver = webdriver.Chrome(cfg['chromeDriverPath'], chrome_options=options)
parseUrl = "https://in.pinterest.com"

while(True):
    driver.get(parseUrl)
    html_doc = driver.page_source
    soup = BeautifulSoup(html_doc, 'html5lib')
    print(soup)