#!/usr/bin/env python3
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
from config.wallmartConfig import config as cfg
import urllib.request
import re
import argparse
import time
from pymongo import MongoClient
from apscheduler.schedulers.blocking import BlockingScheduler
import uuid
import logging

logging.basicConfig(filename="out.log",
                    filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.DEBUG)


def downloadPinterestImages(keyword, file_name, testCol):
    options = Options()
    options.add_argument('--headless')  # comment this line to run on head mode
    options.add_argument('--disable-gpu')
    browser = webdriver.Chrome(cfg['chromeDriverPath'], chrome_options=options)
    link = "https://in.pinterest.com/search/pins/?q="+keyword
    browser.get(link)
    time.sleep(2)
    lenOfPage = browser.execute_script(
        "window.scrollTo(0, document.body.scrollHeight);var lenOfPage=document.body.scrollHeight;return lenOfPage;")
    match = False
    limit = 9
    while(match == False and limit > 0):
        lastCount = lenOfPage
        time.sleep(3)
        lenOfPage = browser.execute_script(
            "window.scrollTo(0, document.body.scrollHeight);var lenOfPage=document.body.scrollHeight;return lenOfPage;")
        limit = limit-1
        if lastCount == lenOfPage:
            logging.info("stop scrolling")
            match = True
        else:
            logging.info("scrolling..")

    response = browser.page_source

    toDel = []
    urls = re.findall(
        'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', response)

    logging.info(len(urls))
    for i in range(len(urls)):
        if(urls[i][-4:] == ".jpg"):
            urls[i] = re.sub('.com/.*?/', '.com/originals/',
                             urls[i], flags=re.DOTALL)
        else:
            urls[i] = ""
    urls = list(set(urls))
    urls = list(filter(None, urls))  # fastest
    urls = list(filter(bool, urls))  # fastest
    urls = list(filter(len, urls))  # a bit slower
    for url in urls:
        obj = {
            "Top": {
                "box": [],
                "style": "",
                "pattern": "",
                "length": "",
                "colors": {"LAB": []},
                "material": "",
                "detail": ""
            },
            "name": 'image'+str(uuid.uuid4())+".jpg",
            "Bottom": {
                "box": [],
                "colors": {"LAB": []},
                "type": "",
                "pattern": "",
                "length": "",
                "material": "",
                "detail": "",
                "skirt_type": ""
            },
            "img_url": url,
            "verified_tags": {
                "verified_tags": [
                    {
                        "top": {
                            "style": "",
                            "sleeve": "",
                            "pattern": "",
                            "material": "",
                            "length": "",
                                        "detail": "",
                                        "colors": {"colors": {"LAB": []}}
                        },
                        "bottom": {
                            "type": "",
                            "fit": "",
                            "pattern": "",
                            "material": "",
                                        "detail": "",
                                        "length": "",
                                        "colors": {"colors": {"LAB": []}}
                        },
                        "inappropriate": False,
                        "swimwear": False,
                        "sleepwear": False,
                        "Celebrity": False,
                        "logo": False,
                        "notwoman": False
                    },
                    {
                        "top": {
                            "style": "",
                            "neck": "",
                            "sleeve": "",
                            "detail": "",
                            "pattern": "",
                            "length": "",
                            "material": "",
                                        "colors": {"colors": {"LAB": []}}
                        },
                        "bottom": {
                            "type": "",
                            "length": "",
                            "skirt_type": "",
                            "detail": "",
                            "material": "",
                                        "pattern": "",
                                        "colors": {"colors": {"LAB": []}}
                        },
                        "inappropriate": False,
                        "swimwear": False,
                        "sleepwear": False,
                        "Celebrity": False,
                        "logo": False,
                        "notwoman": False
                    }
                ],
                "verified_by": []
            },
            "reviewed_tags": {
                "top": {"style": "", "neck": "", "sleeve": "", "length": "", "pattern": "", "detail": "", "material": ""},
                "bottom": {"ankle": "", "fit": "", "type": "", "length": "", "pattern": "", "detail": "", "material": ""},
                "reviewed_by": ""
            },
            "classification": {
                "classified": {"top": 0, "bottom": 0},
                "on_classification": {"top": 0, "bottom": 0},
                "classified_as": {"top": "", "bottom": ""}
            },
            "common": {
                "color": {
                    "top": {"done": 0, "on_process": 0, "value": ""},
                    "bottom": {"done": 0, "on_process": 0, "value": ""}
                }
            },
            "features": {
                "featured": {
                    "top": {"neck": 0, "sleeve": 0, "length": 0, "pattern": 0, "detail": 0, "material": 0},
                    "bottom": {"ankle": 0, "fit": 0, "type": 0, "length": 0, "pattern": 0, "detail": 0, "material": 0}
                },
                "on_features": {
                    "top": {"neck": 0, "sleeve": 0, "length": 0, "pattern": 0, "detail": 0, "material": 0},
                    "bottom": {"ankle": 0, "fit": 0, "type": 0, "length": 0, "pattern": 0, "detail": 0, "material": 0}
                },
                "featured_as": {
                    "top": {"neck": "", "sleeve": "", "length": "", "pattern": "", "detail": "", "material": ""},
                    "bottom": {"ankle": "", "fit": "", "type": "", "length": "", "pattern": "", "detail": "", "material": ""}
                }
            },
            "validity": {"validated": 0, "on_validation": 0, "invalid": 0, "reason": ""}
        }
        # testCol.insert_one( obj)
        testCol.update_one({"img_url": url}, {"$set": obj}, upsert=True)
    logging.info("done and done")


def main_load():
    parser = argparse.ArgumentParser()
    parser.add_argument('--search')
    args = parser.parse_args()
    load = ["men's wear", "men's fashion", "men's clothes", "men's garment", "men's apparel", "david beckham", "men's photoshoot", "men's apparel design", "men fashion show", "men fashion show runway", "ermenegildo zegna 2019", "men formal style", "Mens winter fashion 2019",
            "george clooney style", "gandy david style", "Gq men", "gq mens style casual", "gq mens style casual", "tyson beckford style", "eric rutherford style", "men hipster", "men black outfits", "men outfits classy", "gq mens style", "Chris Hemsworth", "Vogue men"]

    logging.info("job started")
    client = MongoClient(cfg["database"])
    db = client.test
    testCol = db.data_socials
    for i in load:
        i.replace(" ", "%20")
        downloadPinterestImages(i, args.search, testCol)


main_load()
if __name__ == '__main__':
    logging.info("job will run in 1 min")
    scheduler = BlockingScheduler()
    scheduler.add_job(main_load, 'interval', hours=1)
    scheduler.start()
# python3 some.py --search "men top"
