import urllib.request
import re
import argparse
import time

from config.wallmartConfig import config as cfg
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

def downloadPinterestImages(keyword,file_name):
    options = Options()
    # options.add_argument('--headless')  # comment this line to run on head mode
    options.add_argument('--disable-gpu')
    browser = webdriver.Chrome(cfg['chromeDriverPath'], chrome_options=options)
    link="https://in.pinterest.com/search/pins/?q="+keyword
    browser.get(link)
    time.sleep(2)
    lenOfPage = browser.execute_script("window.scrollTo(0, document.body.scrollHeight);var lenOfPage=document.body.scrollHeight;return lenOfPage;")
    match=False
    limit=9
    while(match==False and limit>0): 
        lastCount = lenOfPage
        time.sleep(3)
        lenOfPage = browser.execute_script("window.scrollTo(0, document.body.scrollHeight);var lenOfPage=document.body.scrollHeight;return lenOfPage;")
        limit = limit-1;
        if lastCount==lenOfPage:
            print("stop scrolling")
            match=True
        else:
            print("scrolling..")

    response = browser.page_source

    toDel =[]
    urls = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', response)

    print(len(urls))
    for i in range(len(urls)):
        if(urls[i][-4:]==".jpg"):
            urls[i]=re.sub('.com/.*?/','.com/originals/',urls[i],flags=re.DOTALL)
        else:
            urls[i]= ""
    urls = list(set(urls))
    urls = list(filter(None, urls)) # fastest
    urls = list(filter(bool, urls)) # fastest
    urls = list(filter(len, urls))  # a bit slower

    with open('{0}.txt'.join(file_name), 'w') as f:
        for url in urls:
            f.write("%s\n" % url)
    f.close()
    print("done and done")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--search')
    args = parser.parse_args()
    print(args)
    test = args.search
    test.replace(" ","%20")
    downloadPinterestImages(test,args.search)

#python3 some.py --search "men top"
