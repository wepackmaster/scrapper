from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time

options = Options()
# options.add_argument('--headless')  # comment this line to run on head mode
options.add_argument('--disable-gpu')

driver = webdriver.Chrome("./chrome/chromedriver", chrome_options=options)
driver.get("https://www.youtube.com/watch?v=gXpzn8PAScw")

def watchvideo():
   
    driver.refresh()
    time.sleep(170)
    driver.close()

watchvideo()