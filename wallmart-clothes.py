#!/usr/bin/env python3
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
from pymongo import MongoClient
from config.wallmartConfig import config as cfg
import uuid


client = MongoClient(cfg["database"])
db = client.wallproduct
testCol = db.wall_new_men1
options = Options()
options.add_argument('--headless')  # comment this line to run on head mode
options.add_argument('--disable-gpu')
driver = webdriver.Chrome(cfg['chromeDriverPath'], chrome_options=options)
parseUrl = cfg["parseUrl"]
while(True):
    driver.get(parseUrl)
    html_doc = driver.page_source
    soup = BeautifulSoup(html_doc, 'html5lib')
    nextUrl = soup.select_one(
        'ul:is(.paginator-list)')
    nextUrl = nextUrl.select_one('li:is(.active)')
    parseUrl = cfg["baseUrl"] + \
        nextUrl.find_next_sibling('li').findChild('a')['href']
    containerDiv = soup.select(
        'div:is(.search-result-gridview-item.clearfix.arrange-fill)')

    for div in containerDiv:
        try:
            productName = div.select_one(
                'span:is(.product-brand)').find_next_sibling('span').text if(div) else None
            imageUrl = div.find('img')['src'] if(div) else None
            pageUrl = div.find('a')['href'] if(div) else None
            if(div):
                url = cfg["baseUrl"]+pageUrl
                driver.get(url)
                clothe = driver.page_source
                soupData = BeautifulSoup(clothe, 'html5lib')
                imgData = soupData.select_one(
                    'div:is(.prod-ProductImage.prod-MobileEnhancedCarousel)')
                imgProduct = imgData.find_all('img') if(imgData) else []
                imgArr = list(map(lambda d: d['src'][2:], imgProduct))
                productData = soupData.select_one(
                    'div:is(.hf-Bot.hf-PositionedRelative)')
                brand = soupData.select_one(
                    'p:is(.fashion-brand-name)').findChild('a').text if(soupData) else None
                productPrice = soupData.select_one(
                    'span:is(.price-characteristic)') if(soupData) else None
                productPrice = productPrice['content'] if(
                    productPrice['content']) else None
                color = soupData.select_one(
                    'span:is(.varslabel__content)') if(soupData) else None
                color = color.text if(color) else None
                prodcutColor = soupData.select_one(
                    'div:is(.variant-options-container)') if(soupData) else None
                prodcutColor = prodcutColor.select(
                    'span:is(.variant-option-outer-container)') if(prodcutColor) else None
                prodColorArr = list(
                    map(lambda d: d['aria-label'], prodcutColor)) if(prodcutColor) else None
                id = str(uuid.uuid4())
                print(productName)
                obj = {
                    "id": id,
                    "brand": brand,
                    "title": productName,
                    "color": color,
                    "source": "wallmart",
                    "category": cfg["category"],
                    "type": cfg["type"],
                    "gender": cfg["gender"],
                    "pin_id":None,
                    "url": url,
                    "sizes":[],
                    "from" : "walmart",
                    "variants": imgArr,
                    "meta": {
                        "alternate_image": imgArr,
                        "brand": brand,
                        "image_url": imageUrl,
                        "name": productName,
                        "sizes":[],
                        "price": productPrice,
                        "url": url
                    }
                   
                }
                if(id):
                    testCol.update({'id': id}, obj, True)
                    print("saved the " + productName)
        except Exception as e:
            print(e)

driver.quit()
