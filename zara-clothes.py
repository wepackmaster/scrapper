from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
from pymongo import MongoClient
from config.zaraConfig import config as cfg
import re


client = MongoClient(cfg["database"])
db = client.test_database
testCol = db.common
options = Options()
options.add_argument('--headless')  # comment this line to run on head mode
options.add_argument('--disable-gpu')
driver = webdriver.Chrome(cfg['chromeDriverPath'], chrome_options=options)
parseUrl = cfg["parseUrl"]
driver.get(parseUrl)
html_doc = driver.page_source
soup = BeautifulSoup(html_doc, 'html5lib')
selectedCategory = soup.select_one(
    'li:is(._category-link-wrapper.menu-item.menu-item--level-1.menu-item--current)')
selectedLinkArr = selectedCategory.select(
    'li:is(._category-link-wrapper.menu-item.menu-item--level-2.menu-item--is-leaf)')
selectedLinkArr = list(
    map(lambda d: d.select_one('a'), selectedLinkArr))
selectedLinkedArr = list(
    map(lambda d: d['href'] if d.has_attr('href') else d['data-href'], selectedLinkArr))
for parseUrl in selectedLinkedArr:
    driver.get(parseUrl)
    html_doc = driver.page_source
    soup = BeautifulSoup(html_doc, 'html5lib')
    dataList = soup.findAll('li', id=re.compile('^product-'))
    for data in dataList:
        try:
            query = data.find('a')
            url = query['href'] if(query) else None
            tcin = data['id'].split('-')[1]
            if(url):
                driver.get(url)
                html_data = driver.page_source
                brand = "zara"
                parsedUrl = BeautifulSoup(html_data, 'html5lib')
                imageArr = parsedUrl.select('img:is(.image-big._img-zoom)')
                imgArr = list(map(lambda d: d['src'][2:], imageArr))
                name = parsedUrl.select_one('h1:is(.product-name)').text
                price = parsedUrl.select_one('div:is(.price._product-price)')
                price = price.select_one('span').text
                color = parsedUrl.select_one('span:is(._colorName)').text
                obj = {
                    "brand": brand,
                    "color": color,
                    "id": tcin,
                    "meta": {
                        "alternate_image": imgArr,
                        "brand": brand,
                        "brand_brand": brand,
                        "img_url": imgArr[0],
                        "name": name,
                        "orig_alt_images": imgArr,
                        "price": price,
                        "sizes": [],
                        "url": url
                    },
                    "pin_id": None,
                    "sizes": [],
                    "title": name,
                    "url": url,
                    "from": "zara"
                }
                if(tcin):
                    testCol.update({'tcin': tcin}, obj, True)
                    print("saved the " + name)
        except:
            print("no suitable image")
driver.quit()
