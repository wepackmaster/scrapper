from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
from pymongo import MongoClient
from config.hmConfig import config as cfg


client = MongoClient(cfg["database"])
db = client.test_database
testCol = db.common

options = Options()
options.add_argument('--headless')  # comment this line to run on head mode
options.add_argument('--disable-gpu')
driver = webdriver.Chrome(cfg['chromeDriverPath'], chrome_options=options)
count = 30
parseUrl = cfg["parseUrl"]+str(count)
while(True):
    print(parseUrl)
    driver.get(parseUrl)
    html_doc = driver.page_source
    soup = BeautifulSoup(html_doc, 'html5lib')
    listProd = soup.select_one("ul:is(.products-listing.small)")
    prodArr = listProd.select("li:is(.product-item)")
    count = count+30
    for product in prodArr:
        try:
            url = product.select_one("a:is(.item-link)")['href']
            baseurl = cfg['baseUrl']+url
            driver.get(baseurl)
            prod_doc = driver.page_source
            soup_doc = BeautifulSoup(prod_doc, 'html5lib')
            main_div = soup_doc.select_one(
                "div:is(.module.product-description.sticky-wrapper)")
            imageDiv = soup_doc.select_one(
                "div:is(.product-detail-main-image-container)")
            imageUrl = imageDiv.findChild('img')['src']
            name = main_div.select_one(
                "h1:is(.primary.product-item-headline)").text.strip()
            color = main_div.select_one(
                "h3:is(.product-input-label)").text.strip()
            image_section = main_div.find_all("figure")
            imgArr = list(map(lambda d: d.findChild('img')['src'] if(
                d.findChild('img')) else None, image_section))
            tcin = main_div.select_one("h4:is(.art_no)")
            tcin = tcin.findNext("li").text if(tcin) else None
            price = main_div.select_one("span:is(.price-value)").text
            url = baseurl
            obj = {
                "brand": "h&m",
                "color": color,
                "tcin": tcin,
                "meta": {
                    "alternate_image": imgArr,
                    "brand": "h&m",
                    "brand_brand": "h&m",
                    "img_url": imageUrl,
                    "name": name,
                    "orig_alt_images": imgArr,
                    "price": price,
                    "sizes": [],
                    "url": url
                },
                "pin_id": None,
                "sizes": [],
                "title": name,
                "url": url,
                "from": "h&m",
                "category": "top",
                "type": "tshirt"
            }
            if(tcin):
                testCol.update({'tcin': tcin}, obj, True)
                print("saved the " + name)
            parseUrl = cfg["parseUrl"]+str(count)
        except:
            print("No page found")
driver.quit()
