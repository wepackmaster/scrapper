from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from bs4 import BeautifulSoup
from pymongo import MongoClient
from config.myntraConfig import config as cfg
import re
import uuid
import cssutils


client = MongoClient(cfg["database"])
db = client.myntra
testCol = db.myntra_women
options = Options()
options.add_argument('--headless')  # comment this line to run on head mode
options.add_argument('--disable-gpu')
driver = webdriver.Chrome(cfg['chromeDriverPath'], chrome_options=options)
parseUrl = cfg["parseUrl"]
while(True):
    driver.get(parseUrl)
    html_doc = driver.page_source
    soup = BeautifulSoup(html_doc, 'html5lib')
    nextUrl = soup.select_one('li:is(.pagination-next)')
    parseUrl = nextUrl.find('a')['href']
    try:
        listArr = soup.select_one('ul:is(.results-base)').select('li:is(.product-base)')
        for i in listArr:
            pageUrl = i.find('a')['href']
            url =  url = cfg["baseUrl"]+pageUrl
            driver.get(url)
            clothe = driver.page_source
            soupData = BeautifulSoup(clothe, 'html5lib')
            imageArr = soupData.select_one('div:is(.image-grid-container.common-clearfix)')
            brand = soupData.select_one('h1:is(.pdp-title)').text
            title = soupData.select_one('h1:is(.pdp-name.pdp-bb1)').text
            price = soupData.select_one('span:is(.pdp-price)')
            price = price.select_one('strong').text
            alt_images = []
            for i in imageArr:
                urlImage = i.select_one('div:is(.image-grid-image)')
                if(urlImage):
                    style = cssutils.parseStyle(urlImage['style'])
                    url = style['background-image']
                    url = url.replace('url(', '').replace(')', '')
                    alt_images.append(url)
            sizes = ['S','M','L','XL']
            id = str(uuid.uuid4())
            if(len(alt_images)):
                obj = {
                    "id": id,
                    "brand": brand,
                    "title": title,
                    "color": None,
                    "source": "myntra",
                    "category": cfg["category"],
                    "type": cfg["type"],
                    "gender": cfg["gender"],
                    "pin_id":None,
                    "url": url,
                    "sizes":sizes,
                    "from" : "myntra",
                    "variants": alt_images,
                    "meta": {
                        "alternate_image": alt_images,
                        "brand": brand,
                        "image_url": alt_images[0],
                        "name": title,
                        "sizes":[],
                                "price": price,
                                "url": url
                    }
                        
                }
                if(len(alt_images)):
                    testCol.update({'title': title}, obj, True)
                    print("saved the " + title)
    except Exception as e:
            print(e)
driver.quit()
